# README #

Cubes is a public repository for small self-contained python modules 
used by folks in the [Social Computing](http://qcri.org.qa/our-research/social-computing/social-computing) 
group at [Qatar Computing Research Institute](http://qcri.org.qa)

### About Cubes? ###

* Python modules for data science
* Version 1.0

### What is in Cubes so far?
* Named Entity Recognition (uses Stanford NER) -- Sofiane 2017-05-23
* Language detection for short text
* Gender identification from names/screen_names
* Map a pair of (longitude, latitude) to a zone in Qatar.
* Map a pair of (longitude, latitude) to a country.


### Who do I talk to? ###

* Sofiane Abbar (sofiane.abbar@gmail.com)
* Noora Al Emadi (nalemadi@hbku.edu.qa)