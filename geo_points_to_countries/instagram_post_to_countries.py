import countries
import sys, ast

cc = countries.CountryChecker('TM_WORLD_BORDERS/TM_WORLD_BORDERS-0.3.shp')
for line in sys.stdin:
	s = line.split('\t')
	user = ast.literal_eval(s[1])
	country = 'None'
	if s[2] != 'None':
		location = ast.literal_eval(s[2])
		if "latitude" in location.keys() and "longitude" in location.keys():
			x,y = float(location["latitude"]),float(location["longitude"])
			country = cc.getCountry(countries.Point(x,y))
	print s[0]+'\t',str(user['id'])+'\t'+str(country)
