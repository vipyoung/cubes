import countries
import sys

cc = countries.CountryChecker('TM_WORLD_BORDERS/TM_WORLD_BORDERS-0.3.shp')
for line in sys.stdin:
    try:
        x, y = line.split('\t')
        x, y = float(x),float(y)
        print cc.getCountry(countries.Point(x,y)).iso
    except:
        print 'None'
