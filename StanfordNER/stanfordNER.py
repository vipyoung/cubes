"""
author: sofiane
This code uses Stanford NER to extract named entities from text.
The categories of NE are: Persons, Locations, and Organizations.

;Input: text
;Output: dictionary of entities

Dependencies: You need to install pyner at: https://github.com/dat/pyner
"""

import ner

host = '10.2.0.30' # scdev8
port = 9190
tagger = ner.SocketNER(host=host, port=port)
entities = tagger.get_entities('Sofiane and Monisha have deployed Stanford NER on scdev8 at Meeza')
print entities

