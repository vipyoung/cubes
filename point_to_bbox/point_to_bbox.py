from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import json
from collections import defaultdict


def get_zones_dictionary():
    """
    read geojson file
    Returns: dictionay(zone_id: list of polygons)

    """
    with open('qatar_zone_population.geojson') as f:
        data = json.load(f)
        regions = data.get("features")
    zone_dict = defaultdict(list)
    for region in regions:
        zone_id = region.get(u'properties').get(u'ZONE_ID')
        region_polygon = region.get("geometry")
        for c_list in region_polygon['coordinates']:
            for coordinates in c_list:
                polygon = Polygon([[p[1], p[0]] for p in coordinates])
                zone_dict[zone_id].append(polygon)
    return zone_dict


def find_zone(latitude, longitude, zone_dict):
    """
    return zone_id if latitude/longitude is inside a zone in Qatar, if not return -1
    Args:
        latitude: float
        longitude: float
        zone_dict: dictionary

    Returns: zone_id

    """
    fs_point = Point(latitude, longitude)
    for zone_id, polygons_list in zone_dict.items():
        for polygon in polygons_list:
            if polygon.contains(fs_point):
                return zone_id
    return -1

if __name__ == '__main__':
    zone_dict = get_zones_dictionary()
    zone_id = find_zone(latitude=25.272136161, longitude=51.523008355, zone_dict=zone_dict)
    print zone_id